package com.viamatica.algoritmo2;

import java.util.Random;

/** @author julio16.sy@gmail.com **/

public class Algoritmo2 {
    public static void main(String[] args) {
        
        Random r = new Random();
        
        // Genero al azar el tamaño de la clave
        int tamanioClave = (int)(Math.random()*(15-8+1)+8);
        
        // Variables con informacion para generar clave
        String mayuscula = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String minuscula = "abcdefghijklmnopqrstuvwxyz";
        String especial = "1234567890+-?@~_-$#*";
        String relleno = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890+-?@~_-$#*";

        String claveFinal = "";
        
        // Añado obligatoriamente un caracter
        claveFinal = claveFinal + mayuscula.charAt(r.nextInt(25)+1);
        claveFinal = claveFinal + minuscula.charAt(r.nextInt(25)+1);
        claveFinal = claveFinal + especial.charAt(r.nextInt(20)+1);
        
        // Completo tamaño de clave con cualquier valor
        for (int i=4; i<=tamanioClave ; i++) {
            claveFinal = claveFinal + relleno.charAt(r.nextInt(70)+1);
        }
        
        System.out.println("Tamaño Clave: " + tamanioClave);
        System.out.println("Clave Generada: " + claveFinal);
    }
}
