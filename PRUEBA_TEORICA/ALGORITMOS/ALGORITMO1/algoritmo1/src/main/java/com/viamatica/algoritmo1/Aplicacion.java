package com.viamatica.algoritmo1;

import java.util.Scanner;

/** @author julio16.sy@gmail.com **/

public class Aplicacion {
    public static void main(String[] args) {
        
        String cadenaFrase = "El 31 de diciembre es el último día del año";
        String cadenaTeclado;
        String cadenaInvertida = "";
        String cadenaFinal;
        
        Scanner teclado = new Scanner(System.in);
        System.out.print("Ingrese una palabra: ");
        cadenaTeclado = teclado.nextLine(); 

        // For para recorrer cadenaFrase hacia atras
        for (int i = cadenaFrase.length() - 1; i >= 0; i--) {
            // Almaceno la cadena invertida
            cadenaInvertida = cadenaInvertida + cadenaFrase.charAt(i);
        }
        // Reemplazo espacios vacios con palabra ingresada por teclado
	cadenaFinal = cadenaInvertida.replaceAll("\\s+",cadenaTeclado);
        
	System.out.println(cadenaFinal);
        
    }
}